import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
import logging
import xarray as xr
from datetime import datetime, timedelta
import concurrent.futures
import fsspec


#MP function
def get_rows(args):
		i = 0				#time index is always 0

		#unpack args
		variable = args[0]
		variable_is_time_static = args[1]
		columns = list(args[2])
		dictamo = args[3]
		y_size = len(dictamo['coords']['y']['data'])
		x_size = len(dictamo['coords']['x']['data'])

		time = str(dictamo['coords']['time']['data'][i]).replace(" ","T")

		rows = []
		for j in range(y_size):
			y = dictamo['coords']['y']['data'][j]
			for k in range(x_size):
				line = {}
				if variable_is_time_static:
					value = dictamo['data_vars'][variable]['data'][j][k]
				else:
					value = dictamo['data_vars'][variable]['data'][i][j][k]
				if numpy.isnan(value):
					continue
				#formulate result line
				for column_name in columns:
					if (column_name == "time"):
						line[column_name] = time
					elif (column_name == "y"):
						line[column_name] = y
					elif (column_name == "x"):
						line[column_name] = dictamo['coords']['x']['data'][k]
					else:
						line[column_name] = value
				#append result row
				rows.append(line)
		return rows

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_dataset(self.zarr_file)
		# time range in cube: 2006-04-01 ... 2022-09-29
		# x range in cube: -10.72 -10.71 -10.7 ... 36.72 36.73 36.74
		# y range in cube: 47.7 47.69 47.68 ... 30.09 30.08 30.07

	def get_variable_name(self, columns):
		result = None
		for column_name in columns:
			log_to_postgres('[SEMCUBE] Column: ' + str(column_name), logging.DEBUG)
			if (column_name == "x"):
				continue
			if (column_name == "y"):
				continue
			if (column_name == "time"):
				continue
			else:
				result = column_name
		return result

	def execute(self, quals, columns):
		#ds = xr.open_dataset(self.zarr_file)

		#y and x values for Mesogeos
		y_min = 30.06
		y_max = 47.7
		x_min = -10.72
		x_max = 36.75
		variable_name = self.get_variable_name(columns)
		log_to_postgres('[SEMCUBE] Variable Name: ' + str(variable_name), logging.DEBUG)
		# 2006-04-01 ... 2022-09-29
		start_date = datetime.strptime("2006-04-01", "%Y-%m-%d")
		end_date = datetime.strptime("2022-09-29", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				if qual.operator == '<':
					x_max = float(qual.value)
				if qual.operator == '==':
					x_min = float(qual.value)
					x_max = x_min
				if qual.operator == '<=':
					x_max = float(qual.value)
				if qual.operator == '>=':
					x_min = float(qual.value)
			if qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				if qual.operator == '<':
					y_max = float(qual.value)
				if qual.operator == '==':
					y_min = float(qual.value)
					y_max = y_min
				if qual.operator == '<=':
					y_max = float(qual.value)
				if qual.operator == '>=':
					y_min = float(qual.value)
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				if qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				if qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
				if qual.operator == '>=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				if qual.operator == '<=':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
		#check for static variables (ignore time dimension)
		time_static_vars = {'x', 'y', 'aspect', 'curvature', 'dem', 'roads_distance', 'slope'}
		if columns.issubset(time_static_vars):
			end_date = start_date
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = self.ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)

		variable_is_time_static = (variable_name in time_static_vars)

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished Sel', logging.DEBUG)

			if variable_name is not None:
				#process and create chunks
				chunks = self.get_chunks(ds_subset)
				#spawn parallel processes to read chunked data
				args = ((variable_name, variable_is_time_static, columns, c) for c in chunks)
				with concurrent.futures.ProcessPoolExecutor() as executor:
					results = executor.map(get_rows, args)
					log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
					for res in results:
						for item in res:
							#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
							yield item

	def get_chunks(self, ds_subset):
		i = 0				#time index is always 0
		chunks = []

		x_size = ds_subset.x.size
		y_size = ds_subset.y.size
		log_to_postgres('[SEMCUBE] Creating chunks for: X Size: ' + str(x_size) + ", Y Size: " + str(y_size), logging.DEBUG)

		X_CHUNKS = 8
		x_min_chunked = 0
		x_chunk_size = (x_size//X_CHUNKS) if (x_size % X_CHUNKS == 0) else (x_size//X_CHUNKS + 1)
		x_flag = False

		#chunking at a multiple of available CPU cores (32)
		while not x_flag:
			if x_min_chunked + x_chunk_size >= x_size:
				x_chunk_size = x_size - x_min_chunked
				x_flag = True

			x_subset = ds_subset.isel(x=slice(x_min_chunked, x_min_chunked+x_chunk_size))
			#log_to_postgres('[SEMCUBE] Current x chunk:' + str(x_min_chunked) + " to " + str(x_min_chunked+x_chunk_size), logging.DEBUG)

			Y_CHUNKS = 8
			y_min_chunked = 0
			y_chunk_size = (y_size//Y_CHUNKS) if (y_size % Y_CHUNKS == 0) else (y_size//Y_CHUNKS + 1)
			y_flag = False
			while not y_flag:
				if y_min_chunked + y_chunk_size >= y_size:
					y_chunk_size = y_size - y_min_chunked
					y_flag = True

				y_subset = x_subset.isel(y=slice(y_min_chunked, y_min_chunked+y_chunk_size))
				#log_to_postgres('[SEMCUBE] Current y chunk:' + str(y_min_chunked) + " to " + str(y_min_chunked+y_chunk_size), logging.DEBUG)
				chunks.append(y_subset.to_dict())
				#set new chunk for y
				y_min_chunked += y_chunk_size
			#set new chunk for x
			x_min_chunked += x_chunk_size
		return chunks
