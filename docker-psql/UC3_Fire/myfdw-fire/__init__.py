from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres

import logging
import xarray as xr
import numpy
import pickle
import concurrent.futures
from datetime import datetime, timedelta
from os.path import exists
from pyproj import Transformer

#self.fipool -> Fipool Directory

#MP function
def get_rows(args):
		#to convert lat and lon coordinates to epsg:4326 (from epsg:3024)
		tr = Transformer.from_crs("epsg:3034", "epsg:4326")
		i = 0				#time index is always 0
		
		#unpack args
		variable = args[0]
		columns = list(args[1])
		dictamo = args[2]
		y_size = len(dictamo['coords']['y']['data'])
		x_size = len(dictamo['coords']['x']['data'])

		rows = []
		for j in range(y_size):
			paris = dictamo['data_vars'][variable]['data'][i][j]
			for k in range(x_size):
				line = {}
				value = paris[k]
				if numpy.isnan(value):
					continue
				#formulate result line
				for column_name in columns:
					if (column_name == "time"):
						line[column_name] = str(dictamo['coords']['time']['data'][i]).replace(" ","T")
					elif (column_name == "y"):
						line[column_name],_ = tr.transform(dictamo['coords']['y']['data'][j],dictamo['coords']['x']['data'][k])
					elif (column_name == "x"):
						_,line[column_name] = tr.transform(dictamo['coords']['y']['data'][j],dictamo['coords']['x']['data'][k])
					else:
						line[column_name] = value
				#append result row
				rows.append(line)
		return rows

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.fipool = fdw_options.get('fipool', None)
		if self.fipool is None:
			raise ValueError('The fipool option is mandatory')


	def get_variable_name(self, columns):
		result = None
		for column_name in columns:
			log_to_postgres('[SEMCUBE] Column: ' + str(column_name), logging.DEBUG)
			if (column_name == "x"):
				continue
			elif (column_name == "y"):
				continue
			elif (column_name == "time"):
				continue
			else:
				result = column_name
		return result

	def execute(self, quals, columns):

		#y and x bounds
		x_min = 18.30674907856706
		x_max = 31.171264677565883
		y_min = 32.678588843604935
		y_max = 43.494990430557515

		#2022-07-01 ... 2022-10-31
		start_date = datetime.strptime("2022-07-08", "%Y-%m-%d")
		end_date = datetime.strptime("2022-12-31", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)

		#Variables for time calculations
		op_index = 0
		req_month = ""
		req_day = ""

		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				elif qual.operator == '<':
					x_max = float(qual.value)
			elif qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				elif qual.operator == '<':
					y_max = float(qual.value)
			elif qual.field_name == 'time':
				if qual.operator == '>':
					op_index = 1
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				elif qual.operator == ">=":
					op_index = 2
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '<':
					op_index = 3
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				elif qual.operator == "<=":
					op_index = 4
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '=':
					op_index = 5
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		#convert coordinate bounds to epsg:3034 (from epsg:4326)
		tr = Transformer.from_crs("epsg:4326", "epsg:3034")
		y_max, x_min = tr.transform(y_max, x_min)
		y_min, x_max = tr.transform(y_min, x_max)
		y_min = float(round(y_min))
		y_max = float(round(y_max))
		x_min = float(round(x_min))
		x_max = float(round(x_max))
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) , logging.DEBUG)
		log_to_postgres('[SEMCUBE] Time Range: [' + str(start_date) + ',' + str(end_date) + ']', logging.DEBUG)

		#get variable name
		variable_name = self.get_variable_name(columns)

		#find netcdf files to retrieve requested var
		if op_index <= 2:
			req_year = str(start_date.year)
			req_month = str(start_date.month).zfill(2)
			req_day = str(start_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			m_prefix = self.fipool + req_year + '/' + req_month + '/'
			reqday_path = m_prefix + reqfile_day
			reqnight_path = m_prefix + reqfile_night
			#check if file path exists
			while exists(m_prefix):
				#pick which dataset to open based on requested variable
				if variable_name == "lst_night" or variable_name == "dummy":
					#check if file exists
					if exists(reqnight_path):
						#open and slice dataset
						ds = xr.open_dataset(reqnight_path)
						sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
						log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
						#process and create chunks
						chunks = self.get_chunks(sliced_ds)
						#spawn parallel processes to read chunked data
						args = ((variable_name, columns, c) for c in chunks)
						with concurrent.futures.ProcessPoolExecutor() as executor:
							results = executor.map(get_rows, args)
							log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
							for res in results:
								for item in res:
									#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
									yield item
					else:
						log_to_postgres('[SEMCUBE] Requested night EO file does not exist', logging.DEBUG)
				elif variable_name is not None:
					#log_to_postgres('[SEMCUBE] Reqday path: ' + reqday_path, logging.DEBUG)
					#check if file exists
					if exists(reqday_path):
						#open and slice dataset
						ds = xr.open_dataset(reqday_path)
						sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
						log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
						#process and create chunks
						chunks = self.get_chunks(sliced_ds)
						#spawn parallel processes to read chunked data
						args = ((variable_name, columns, c) for c in chunks)
						with concurrent.futures.ProcessPoolExecutor() as executor:
							results = executor.map(get_rows, args)
							log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
							for res in results:
								for item in res:
									#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
									yield item
					else:
						log_to_postgres('[SEMCUBE] Requested day EO file does not exist', logging.DEBUG)
				#get the next day
				start_date = start_date + timedelta(days=1)
				req_month = str(start_date.month).zfill(2)
				req_day = str(start_date.day).zfill(2)
				reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
				reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
				reqday_path = m_prefix + reqfile_day
				reqnight_path = m_prefix + reqfile_night
				m_prefix = self.fipool + req_year + '/' + req_month + '/'

		elif op_index <= 4:
			req_year = str(end_date.year)
			req_month = str(end_date.month).zfill(2)
			req_day = str(end_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			m_prefix = self.fipool + req_year + '/' + req_month + '/'
			reqday_path = m_prefix + reqfile_day
			reqnight_path = m_prefix + reqfile_night
			#check if file path exists
			while exists(m_prefix):
				#pick which dataset to open based on requested variable
				if variable_name == "lst_night" or variable_name == "dummy":
					#check if file exists
					#log_to_postgres('[SEMCUBE] Reqnight path: ' + reqnight_path, logging.DEBUG)
					if exists(reqnight_path):
						#open and slice dataset
						ds = xr.open_dataset(reqnight_path)
						sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
						log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
						#process and create chunks
						chunks = self.get_chunks(sliced_ds)
						#spawn parallel processes to read chunked data
						args = ((variable_name, columns, c) for c in chunks)
						with concurrent.futures.ProcessPoolExecutor() as executor:
							results = executor.map(get_rows, args)
							log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
							for res in results:
								for item in res:
									#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
									yield item
					else:
						log_to_postgres('[SEMCUBE] Requested night EO file does not exist', logging.DEBUG)
				elif variable_name is not None:
					#check if file exists
					if exists(reqday_path):
						#open and slice dataset
						ds = xr.open_dataset(reqday_path)
						sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
						log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
						#process and create chunks
						chunks = self.get_chunks(sliced_ds)
						#spawn parallel processes to read chunked data
						args = ((variable_name, columns, c) for c in chunks)
						with concurrent.futures.ProcessPoolExecutor() as executor:
							results = executor.map(get_rows, args)
							log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
							for res in results:
								for item in res:
									#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
									yield item
					else:
						log_to_postgres('[SEMCUBE] Requested day EO file does not exist', logging.DEBUG)
				#get the previous day
				end_date = end_date - timedelta(days=1)
				req_month = str(end_date.month).zfill(2)
				req_day = str(end_date.day).zfill(2)
				reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
				reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
				reqday_path = m_prefix + reqfile_day
				reqnight_path = m_prefix + reqfile_night
				m_prefix = self.fipool + req_year + '/' + req_month + '/'

		elif op_index == 5:
			req_year = str(start_date.year)
			req_month = str(start_date.month).zfill(2)
			req_day = str(start_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			m_prefix = self.fipool + req_year + '/' + req_month + '/'
			reqday_path = m_prefix + reqfile_day
			reqnight_path = m_prefix + reqfile_night
			#pick which dataset to open based on requested variable
			if variable_name == "lst_night" or variable_name == "dummy":
				#check if file exists
				if exists(reqnight_path):
					#open and slice dataset
					ds = xr.open_dataset(reqnight_path)
					sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
				else:
					log_to_postgres('[SEMCUBE] Requested night EO file does not exist', logging.DEBUG)
					exit(-7)
			elif variable_name is not None:
				#check if file exists
				if exists(reqday_path):
					#open and slice dataset
					ds = xr.open_dataset(reqday_path)
					sliced_ds = ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
				else:
					log_to_postgres('[SEMCUBE] Requested day EO file does not exist', logging.DEBUG)
					exit(-8)
			log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
			#process and create chunks
			chunks = self.get_chunks(sliced_ds)
			#spawn parallel processes to read chunked data
			args = ((variable_name, columns, c) for c in chunks)
			with concurrent.futures.ProcessPoolExecutor() as executor:
				results = executor.map(get_rows, args)
				log_to_postgres('[SEMCUBE] Yielding rows...', logging.DEBUG)
				for res in results:
					for item in res:
						#log_to_postgres('[SEMCUBE] Yielding row: ' + str(item), logging.DEBUG)
						yield item


	def get_chunks(self, sliced_ds):
		#to convert lat and lon coordinates to epsg:4326 (from epsg:3024)
		tr = Transformer.from_crs("epsg:3034", "epsg:4326")
		i = 0				#time index is always 0
		chunks = []

		x_size = sliced_ds.x.size
		y_size = sliced_ds.y.size
		log_to_postgres('[SEMCUBE] Creating chunks for: X Size: ' + str(x_size) + ", Y Size: " + str(y_size), logging.DEBUG)

		X_CHUNKS = 8
		x_min_chunked = 0
		x_chunk_size = (x_size//X_CHUNKS) if (x_size % X_CHUNKS == 0) else (x_size//X_CHUNKS + 1)
		x_flag = False

		#chunking at a multiple of available CPU cores (32)
		while not x_flag:
			if x_min_chunked + x_chunk_size >= x_size:
				x_chunk_size = x_size - x_min_chunked
				x_flag = True

			x_subset = sliced_ds.isel(x=slice(x_min_chunked, x_min_chunked+x_chunk_size))
			#log_to_postgres('[SEMCUBE] Current x chunk:' + str(x_min_chunked) + " to " + str(x_min_chunked+x_chunk_size), logging.DEBUG)

			Y_CHUNKS = 8
			y_min_chunked = 0
			y_chunk_size = (y_size//Y_CHUNKS) if (y_size % Y_CHUNKS == 0) else (y_size//Y_CHUNKS + 1)
			y_flag = False
			while not y_flag:
				if y_min_chunked + y_chunk_size >= y_size:
					y_chunk_size = y_size - y_min_chunked
					y_flag = True

				y_subset = x_subset.isel(y=slice(y_min_chunked, y_min_chunked+y_chunk_size))
				#log_to_postgres('[SEMCUBE] Current y chunk:' + str(y_min_chunked) + " to " + str(y_min_chunked+y_chunk_size), logging.DEBUG)
				chunks.append(y_subset.to_dict())
				#set new chunk for y
				y_min_chunked += y_chunk_size
			#set new chunk for x
			x_min_chunked += x_chunk_size
		return chunks