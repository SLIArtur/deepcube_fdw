import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-raptor-slovenia',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-raptor-slovenia'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
