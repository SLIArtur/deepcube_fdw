import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-raptor-greece',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-raptor-greece'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
