import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
import logging
import xarray as xr
from datetime import datetime, timedelta
import timeit
import shapely.wkt
from shapely.geometry import LineString,MultiLineString
import psycopg2
from psycopg2 import sql
import json

#self.zarr_file -> uc3/uc3cube.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_dataset(self.zarr_file)
		
	def get_path_keys(self):
		return [(('geom',), 1)]

	def execute(self, quals, columns):
		'''
		#y and x values for Mesogeos (Greece)
		y_min = 30.06
		y_max = 47.7
		x_min = -10.72
		x_max = 36.75
		'''
		# 2006-04-01 ... 2022-09-29
		start_date = datetime.strptime("2006-04-01", "%Y-%m-%d")
		end_date = datetime.strptime("2022-09-29", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		y_min = 99.99
		y_max = -99.99
		x_min = 99.99
		x_max = -99.99
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			#log_to_postgres('[SEMCUBE] Qual is: ' + str(qual.field_name), logging.DEBUG)
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				elif qual.operator == '>=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				elif qual.operator == '<=':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
			if qual.field_name == 'variable_name':
				if qual.operator == "=":
					cube_variable = qual.value
			if qual.field_name == 'aggregate_function':
				if qual.operator == "=":
					aggr_function = qual.value
			if qual.field_name == 'spatial_relation':
				if qual.operator == "=":
					spatial_relation = qual.value
			if qual.field_name == 'geom':
				if qual.operator == '=':
					geom_wkt = [qual.value]
					#log_to_postgres('[SEMCUBE] Read WKT geometry', logging.DEBUG, qual.value)
					coords_list = []
					polygons = []
					log_to_postgres('[SEMCUBE] Geometry is: ' + str(geom_wkt), logging.DEBUG)
					for shape in geom_wkt:
						shapelyObject = shapely.wkt.loads(shape)
						if shapelyObject.geom_type == 'Polygon':
							start = timeit.default_timer()
							coords = list(shapelyObject.exterior.coords)
							polygons.append(shapelyObject)
							log_to_postgres('[SEMCUBE] Extracted polygon coordinates:\n\n', logging.DEBUG, ", ".join(map(str, coords)))
							coords_list.append(coords)
							for pol in coords_list:
								for tup in pol:
									if tup[0] < x_min:
										x_min = tup[0]
									if tup[0] > x_max:
										x_max = tup[0]
									if tup[1] < y_min:
										y_min = tup[1]
									if tup[1] > y_max:
										y_max = tup[1]
							stop = timeit.default_timer()
							log_to_postgres('[SEMCUBE] Time to find Polygon MBR: ' + str(stop - start), logging.DEBUG)
						elif shapelyObject.geom_type == 'MultiPolygon':
							start = timeit.default_timer()
							for polygon in shapelyObject:
									coords = list(polygon.exterior.coords)
									polygons.append(polygon)
									log_to_postgres('[SEMCUBE] Extracted polygon coordinates:\n\n', logging.DEBUG, ", ".join(map(str, coords)))
									coords_list.append(coords)
							#iterate over each polygon to find the lon,lat bounds
							for pol in coords_list:
								for tup in pol:
									if tup[0] < x_min:
										x_min = tup[0]
									if tup[0] > x_max:
										x_max = tup[0]
									if tup[1] < y_min:
										y_min = tup[1]
									if tup[1] > y_max:
										y_max = tup[1]
							stop = timeit.default_timer()
							log_to_postgres('[SEMCUBE] Time to find Multipolygon MBR: ' + str(stop - start), logging.DEBUG)
							#TODO(?): Use <envelope, bounds> methods of shapely to find MBR instead
						else:
							raise ValueError('Unsupported geometry: ' + geom_wkt)

		#check for static variables (ignore time dimension)
		time_static_vars = {'x', 'y', 'aspect', 'curvature', 'dem', 'roads_distance', 'slope'}
		variable_is_time_static = False
		if cube_variable in time_static_vars:
			end_date = start_date
			variable_is_time_static = True


		#check if data available in cache
		use_cache = False
		f = open('/docker-psql_test/cache_info/cache_mesogeos.json')
		cache_data = json.load(f)
		log_to_postgres('[SEMCUBE] Cache data: ' + str(cache_data), logging.DEBUG)
		f.close

		#get cached time interval
		max_date, min_date = max(cache_data), min(cache_data)
		log_to_postgres('[SEMCUBE] Min date: ' + str(min_date), logging.DEBUG)
		log_to_postgres('[SEMCUBE] Max date: ' + str(max_date), logging.DEBUG)
		cmin_date, cmax_date = datetime.strptime(min_date, "%Y-%m-%dT%H:%M:%S"), datetime.strptime(max_date, "%Y-%m-%dT%H:%M:%S")

		#get cached variable
		cvar = cache_data[min_date][0]
		log_to_postgres('[SEMCUBE] Variable: ' + str(cvar), logging.DEBUG)

		#decide whether to use case
		if (cmin_date <= start_date and cmax_date >= end_date and cvar == cube_variable):
			use_cache = True

		log_to_postgres('[SEMCUBE] Using cache: ' + str(use_cache), logging.DEBUG)	

		#use_cache = False
		if (use_cache):
			#postgres db details
			db_settings = {
				"database": 'cubez',
				"user": 'postgres',
				"password": 'avni32',
				"host": '88.197.53.173',
				"port": '86'
			}

			#initialize connection
			conn = psycopg2.connect(**db_settings)
			#create cursor object
			cur = conn.cursor()

			#get data from the sliced datacube
			y_query = sql.SQL("select time, y, x, {variable} from cache_mesogeos where x >= %s and x <= %s and y >= %s and y <= %s and time >= %s and time <= %s").format(
								variable=sql.Identifier(cube_variable),
							)
			data_y = (x_min, x_max, y_min, y_max, start_date.strftime("%Y-%m-%dT%H:%M:%S"), end_date.strftime("%Y-%m-%dT%H:%M:%S"))

			cur.execute(y_query, data_y)
			res_data = cur.fetchall()

			#get y coordinates
			y_coords = set()
			for res in res_data:
				y_coords.add(res[1])

			#log_to_postgres('[SEMCUBE] Fetched y coordinates:\n\n', logging.DEBUG, ", ".join(map(str, y_coords)))

			#initializations
			res_line = {}
			result = 0.0

			# ~~~~~Raptor join~~~~~
			#find scanline intersections
			lines_list = []
			start = timeit.default_timer()
			for y in y_coords:
				#construct horizontal line
				scanline = LineString([(0, y), (x_max, y)])
				#compute intersections /w polygons
				for pol in polygons:
					intersection = scanline.intersection(pol)
					log_to_postgres('[SEMCUBE] Computed intersection:\n', logging.DEBUG, str(intersection))
					#sort by x coordinate
					#NOTE: tuples seem to have been sorted automatically; re-check on step 3
					if not intersection.is_empty:
						lines_list.append(intersection)
			#get line coords as strings
			line_coords_list = []
			for lines in lines_list:
				l_coords = []
				#account for multiple lines
				if type(lines) is MultiLineString:
					for l in lines:
						l_coords.append(list(l.coords))
				elif type(lines) is LineString:
					l_coords.append(list(lines.coords))
				log_to_postgres('[SEMCUBE] Extracted line coordinates:\n\n', logging.DEBUG, ", ".join(map(str, l_coords)))
				line_coords_list.append(l_coords)
			stop = timeit.default_timer()
			log_to_postgres('[SEMCUBE] Time to find scanline intersections: ' + str(stop - start), logging.DEBUG)


			while start_date <= end_date:
				time = start_date.strftime("%Y-%m-%dT%H:%M:%S")
				log_to_postgres('[SEMCUBE] Present date: ' + time, logging.DEBUG)

				#perform join on given geometry area
				if spatial_relation == "intersects":
					# ~~~~~Raptor join~~~~~~
					all_values = []
					start = timeit.default_timer()
					for lines in line_coords_list:
						#get observation pixels range based on intersections
						#if sorted, only get x coord. of 0 and -1 elements for min and max, respectively
						#log_to_postgres('[SEMCUBE] Lines is: ' + str(lines), logging.DEBUG)
						for line in lines:
							x_min = line[0][0]
							x_max = line[1][0]
							y_max = line[0][1]
							#get desired interval
							var_values = []
							for row in res_data:
								if (row[2] >= x_min and row[2] <= x_max) and row[1] == y_max and row[0] == time:
									var_values.append(row[3])
							#log_to_postgres('[SEMCUBE] var_values is  ' + str(var_values), logging.DEBUG)
							if var_values:
								#concatenate values
								all_values.append(var_values)
								if variable_is_time_static:
									break

					#calculate aggregate using xarray
					stop = timeit.default_timer()
					log_to_postgres('[SEMCUBE] Time to get all values: ' + str(stop - start), logging.DEBUG)
					#check if no intersections were found or no measurements available
					#log_to_postgres('[SEMCUBE] all_values is ' + str(all_values), logging.DEBUG)
					if not all_values:
						result = numpy.nan
					else:
						final_arr = numpy.concatenate(all_values)
						if aggr_function == "max":
							result = numpy.nanmax(final_arr)
						elif aggr_function == "min":
							result = numpy.nanmin(final_arr)
						elif aggr_function == "count":
							result = numpy.count_nonzero(final_arr)
						elif aggr_function == "sum":
							result = numpy.nansum(final_arr)
						elif aggr_function == "avg":
							result = numpy.nanmean(final_arr)
						else:
							raise ValueError('Unknown Aggregate Function: ' + aggr_function)

				#return nothing if no results were valid
				if numpy.isnan(result):
					return
				#yield result row
				log_to_postgres('[SEMCUBE] Result is: ' + str(result), logging.DEBUG)
				for column_name in columns:
					if (column_name == "time"):
						res_line[column_name] = time
					elif (column_name == "variable_name"):
						res_line[column_name] = cube_variable
					elif (column_name == "aggregate_function"):
						res_line[column_name] = aggr_function
					elif (column_name == "spatial_relation"):
						res_line[column_name] = spatial_relation
					elif (column_name == "geom"):
						res_line[column_name] = geom_wkt[0]
					elif (column_name == "result"):
						res_line[column_name] = result
				yield res_line
				#log_to_postgres('[SEMCUBE] Yielded line: ' + str(res_line), logging.DEBUG)

				#get the next day
				start_date += timedelta(days=1)

			#close connection
			conn.close()

		#standard datacube access (no cache)
		else:
			#slice cube on MBR
			log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
			log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
			sliced_ds = self.ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min),time=slice(start_date, end_date))
			log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)


			#initializations
			res_line = {}
			result = 0.0

			variable_name = cube_variable

			# ~~~~~Raptor join~~~~~
			#find scanline intersections
			ds_subset = sliced_ds.isel(time=[0])
			lines_list = []
			start = timeit.default_timer()
			for j in range(ds_subset.y.size):
				#construct horizontal line
				scanline = LineString([(0, ds_subset.y.values[j]), (x_max, ds_subset.y.values[j])])
				#compute intersections /w polygons
				for pol in polygons:
					intersection = scanline.intersection(pol)
					log_to_postgres('[SEMCUBE] Computed intersection:\n', logging.DEBUG, str(intersection))
					#sort by x coordinate
					#NOTE: tuples seem to have been sorted automatically; re-check on step 3
					if not intersection.is_empty:
						lines_list.append(intersection)
			#get line coords as strings
			line_coords_list = []
			for lines in lines_list:
				l_coords = []
				#account for multiple lines
				if type(lines) is not LineString:
					for l in lines:
						l_coords.append(list(l.coords))
				else:
					l_coords.append(list(lines.coords))
				log_to_postgres('[SEMCUBE] Extracted line coordinates:\n\n', logging.DEBUG, ", ".join(map(str, l_coords)))
				line_coords_list.append(l_coords)
			stop = timeit.default_timer()
			log_to_postgres('[SEMCUBE] Time to find scanline intersections: ' + str(stop - start), logging.DEBUG)

			for timeindex in range(sliced_ds.time.size):
				log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
				ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
				log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
				i = 0 #time index is always 0
				time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
				#time = datetime.strptime(ds_subset.time.values[i], "%Y-%m-%dT%H:%M:%S")
				#return time in this form, otherwise postgres checks f an equality constraint on time is valid and does not return any results

				log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)

				#perform join on given geometry area
				if spatial_relation == "intersects":
					# ~~~~~Raptor join~~~~~~
					all_values = []
					start = timeit.default_timer()
					for lines in line_coords_list:
						#get observation pixels range based on intersections
						#if sorted, only get x coord. of 0 and -1 elements for min and max, respectively
						log_to_postgres('[SEMCUBE] Lines is: ' + str(lines), logging.DEBUG)
						for line in lines:
							x_min = line[0][0]
							x_max = line[1][0]
							y_max = line[0][1]
							#slice subset
							inter_subset = ds_subset.sel(x=slice(x_min,x_max),y=[y_max])
							#get values for requested variable from cube
							value_array = None
							if variable_name is not None:
								value_array = inter_subset.get(variable_name)
								log_to_postgres('[SEMCUBE] Value_array is ' + str(value_array), logging.DEBUG)
							if value_array is not None:
								if variable_is_time_static:
									var_values = value_array.values
								else:
									var_values = value_array.values[i]
								#concatenate values
								log_to_postgres('[SEMCUBE] var_values is ' + str(var_values), logging.DEBUG)
								if var_values.size:
									all_values.append(var_values[0])

					#calculate aggregate using xarray
					stop = timeit.default_timer()
					log_to_postgres('[SEMCUBE] Time to get all values: ' + str(stop - start), logging.DEBUG)
					#check if no intersections were found or no measurements available
					log_to_postgres('[SEMCUBE] all_values is ' + str(all_values), logging.DEBUG)
					if not all_values:
						result = numpy.nan
					else:
						final_arr = numpy.concatenate(all_values)
						if aggr_function == "max":
							result = numpy.nanmax(final_arr)
						elif aggr_function == "min":
							result = numpy.nanmin(final_arr)
						elif aggr_function == "count":
							result = numpy.count_nonzero(final_arr)
						elif aggr_function == "sum":
							result = numpy.nansum(final_arr)
						elif aggr_function == "avg":
							result = numpy.nanmean(final_arr)
						else:
							raise ValueError('Unknown Aggregate Function: ' + aggr_function)

				#return nothing if no results were valid
				if numpy.isnan(result):
					return
				
				#yield result row
				for column_name in columns:
					if (column_name == "time"):
						res_line[column_name] = time
					elif (column_name == "variable_name"):
						res_line[column_name] = cube_variable
					elif (column_name == "aggregate_function"):
						res_line[column_name] = aggr_function
					elif (column_name == "spatial_relation"):
						res_line[column_name] = spatial_relation
					elif (column_name == "geom"):
						res_line[column_name] = geom_wkt[0]
					elif (column_name == "result"):
						res_line[column_name] = result
				yield res_line
				log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)
				
