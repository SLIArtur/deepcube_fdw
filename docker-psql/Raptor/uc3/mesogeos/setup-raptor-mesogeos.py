import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-raptor-mesogeos',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-raptor-mesogeos'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
