import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-mbr-mesogeos',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-mbr-mesogeos'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
