import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
import logging
import xarray as xr
from datetime import datetime, timedelta
import timeit
import shapely.wkt
from shapely.geometry import Point,Polygon

#self.zarr_file -> uc3/uc3cube.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_dataset(self.zarr_file)

	def get_path_keys(self):
		return [(('geom',), 1)]

	def get_variable_name(self, variable_name):
		result = None
		
		log_to_postgres('[SEMCUBE] Variable: ' + variable_name, logging.DEBUG)
		
		if (variable_name == "aster_gdem_dem"):
			result = "ASTER_GDEM_DEM"
		elif (variable_name == "cams_ch4"):
			result = "CAMS_ch4"
		elif (variable_name == "cams_co"):
			result = "CAMS_co"
		elif (variable_name == "cams_co3"):
			result = "CAMS_co3"
		elif (variable_name == "cams_no"):
			result = "CAMS_no"
		elif (variable_name == "cams_no2"):
			result = "CAMS_no2"
		elif (variable_name == "cams_so2"):
			result = "CAMS_so2"
		elif (variable_name == "lc100"):
			result = "LC100"
		elif (variable_name == "ndvi"):
			result = "NDVI"
		elif (variable_name == "ndvi_unc"):
			result = "NDVI_unc"
		elif (variable_name == "swi_001"):
			result = "SWI_001"
		elif (variable_name == "swi_005"):
			result = "SWI_005"
		elif (variable_name == "swi_010"):
			result = "SWI_010"
		elif (variable_name == "swi_015"):
			result = "SWI_015"
		elif (variable_name == "swi_020"):
			result = "SWI_020"
		elif (variable_name == "swi_040"):
			result = "SWI_040"
		elif (variable_name == "swi_060"):
			result = "SWI_060"
		elif (variable_name == "swi_100"):
			result = "SWI_100"
		elif (variable_name == "thermal_mrt"):
			result = "THERMAL_MRT"
		elif (variable_name == "thermal_utci"):
			result = "THERMAL_UTCI"
		else:
			raise ValueError('Unknown Variable: ' + variable_name)
		return result
	
	def execute(self, quals, columns):
		'''
		#lat and lon bounds for Brazil
		lat_min = -6.01
		lat_max = -0.99
		lon_min = -47.01
		lon_max = -41.99
		'''
		# 2019-01-01 ... 2019-03-31
		start_date = datetime.strptime("2019-01-01", "%Y-%m-%d")
		end_date = datetime.strptime("2019-04-01", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		y_min = 99.99
		y_max = -99.99
		x_min = 99.99
		x_max = -99.99
		multipol_flag = False
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(hours=1)
				if qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(hours=-1)
				if qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=59)
			if qual.field_name == 'variable_name':
				if qual.operator == "=":
					cube_variable = qual.value
			if qual.field_name == 'aggregate_function':
				if qual.operator == "=":
					aggr_function = qual.value
			if qual.field_name == 'spatial_relation':
				if qual.operator == "=":
					spatial_relation = qual.value
			if qual.field_name == 'geom':
				if qual.operator == '=':
					if qual.value is None:
						log_to_postgres('[SEMCUBE] Geometry area undefined!', logging.DEBUG)
						return
					geom_wkt = [qual.value]
					#log_to_postgres('[SEMCUBE] Read WKT geometry', logging.DEBUG, qual.value)
					coords_list = []
					polygons = []
					log_to_postgres('[SEMCUBE] Geometry is: ' + str(geom_wkt), logging.DEBUG)
					for shape in geom_wkt:
						shapelyObject = shapely.wkt.loads(shape)
						if shapelyObject.geom_type == 'Polygon':
							start = timeit.default_timer()
							coords = list(shapelyObject.exterior.coords)
							polygons.append(shapelyObject)
							log_to_postgres('[SEMCUBE] Extracted polygon coordinates:\n\n', logging.DEBUG, ", ".join(map(str, coords)))
							coords_list.append(coords)
							for pol in coords_list:
								for tup in pol:
									if tup[0] < x_min:
										x_min = tup[0]
									if tup[0] > x_max:
										x_max = tup[0]
									if tup[1] < y_min:
										y_min = tup[1]
									if tup[1] > y_max:
										y_max = tup[1]
							stop = timeit.default_timer()
							log_to_postgres('[SEMCUBE] Time to find Polygon MBR: ' + str(stop - start), logging.DEBUG)
						elif shapelyObject.geom_type == 'MultiPolygon':
							multipol_flag = True
							start = timeit.default_timer()
							for polygon in shapelyObject:
									coords = list(polygon.exterior.coords)
									polygons.append(polygon)
									log_to_postgres('[SEMCUBE] Extracted polygon coordinates:\n\n', logging.DEBUG, ", ".join(map(str, coords)))
									coords_list.append(coords)
							#iterate over each polygon to find the lon,lat bounds
							for pol in coords_list:
								for tup in pol:
									if tup[0] < x_min:
										x_min = tup[0]
									if tup[0] > x_max:
										x_max = tup[0]
									if tup[1] < y_min:
										y_min = tup[1]
									if tup[1] > y_max:
										y_max = tup[1]
							stop = timeit.default_timer()
							log_to_postgres('[SEMCUBE] Time to find Multipolygon MBR: ' + str(stop - start), logging.DEBUG)
							#TODO(?): Use <envelope, bounds> methods of shapely to find MBR instead
						else:
							raise ValueError('Unsupported geometry: ' + geom_wkt)

		#check for static variables (ignore time dimension)
		time_static_vars = {'lat', 'lon', 'ASTER_GDEM_DEM', 'LC100'}
		variable_is_time_static = False
		if cube_variable in time_static_vars:
			end_date = start_date
			variable_is_time_static = True
		#slice cube on MBR
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = self.ds.sel(lon=slice(x_min,x_max),lat=slice(y_min,y_max),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)


		#initializations
		line = {}
		result = 0.0
		all_values = []

		variable_name = self.get_variable_name(cube_variable)

		cPolygons = []
		if not multipol_flag:
			cPolygon = Polygon(coords_list[0])
			cPolygons.append(cPolygon)
		else:
			for p in coords_list:
				cPolygons.append(Polygon(p))

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
			i = 0 #time index is always 0
			time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
			#time = datetime.strptime(ds_subset.time.values[i], "%Y-%m-%dT%H:%M:%S")
			#return time in this form, otherwise postgres checks f an equality constraint on time is valid and does not return any results
				
			log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)

			#get values for requested variable from cube
			value_array = None
			if variable_name is not None:
				value_array = ds_subset.get(variable_name)
				log_to_postgres('[SEMCUBE] Value_array is ' + str(value_array), logging.DEBUG)
			if value_array is not None:
				if variable_is_time_static:
					var_values = value_array.values
				else:
					var_values = value_array.values[i]
			
			'''
			#replace NaN values with 0s
			if sum_flag:
				var_values[numpy.isnan(var_values)] = 0
			'''

			#perform join on given geometry area
			if spatial_relation == "intersects":
				log_to_postgres('[SEMCUBE] Start aggregating', logging.DEBUG)
				start = timeit.default_timer()
				for j in range(ds_subset.lat.size):
					for k in range(ds_subset.lon.size):
						#log_to_postgres('[SEMCUBE] loop for i: ' + str(i) + ' j:' + str(j) + ' k:' + str(k), logging.DEBUG)
						#log_to_postgres('[SEMCUBE] Numeric measurement found at: ', logging.DEBUG, ", ".join(map(str, (ds_subset.x.values[k], ds_subset.y.values[j]))))
						#check if pixel lies within given polygon
						ctup = (ds_subset.lon.values[k], ds_subset.lat.values[j])
						inter_flag = False
						for cPoly in cPolygons:
							if Point(ctup).intersects(cPoly):
								inter_flag = True
								break
						if inter_flag:
							log_to_postgres('[SEMCUBE] Point in polygon!', logging.DEBUG)
							all_values.append(var_values[j][k])

			#calculate aggregate using numpy
			stop = timeit.default_timer()
			log_to_postgres('[SEMCUBE] Time to get all values: ' + str(stop - start), logging.DEBUG)	
			#check if no intersections were found
			if not all_values:
				result = numpy.nan
			else:
				final_arr = numpy.array(all_values)
				log_to_postgres('[SEMCUBE] Final array is: ' + str(final_arr), logging.DEBUG)
				if aggr_function == "max":
					result = numpy.nanmax(final_arr)
				elif aggr_function == "min":
					result = numpy.nanmin(final_arr)
				elif aggr_function == "count":
					result = numpy.count_nonzero(final_arr)
				elif aggr_function == "sum":
					result = numpy.nansum(final_arr)
				elif aggr_function == "avg":
					result = numpy.nanmean(final_arr)
				else:
					raise ValueError('Unknown Aggregate Function: ' + aggr_function)
			if numpy.isnan(result):
					return


		#yield result row
		for column_name in columns:
			if (column_name == "time"):
				line[column_name] = time
			elif (column_name == "variable_name"):
				line[column_name] = cube_variable
			elif (column_name == "aggregate_function"):
				line[column_name] = aggr_function
			elif (column_name == "spatial_relation"):
				line[column_name] = spatial_relation
			elif (column_name == "geom"):
				line[column_name] = geom_wkt[0]
			elif (column_name == "result"):
				line[column_name] = result
		yield line
		log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)
