import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-mbr-brazil',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-mbr-brazil'],
  install_requires=['xarray', 'netCDF4', 'fsspec']
)
