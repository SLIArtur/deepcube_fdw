import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres

import logging
import xarray as xr
from datetime import datetime, timedelta
import json
import time


#self.zarr_file -> predictions.nc

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.graph_file = fdw_options.get('graph_file', None)
		if self.graph_file is None:
			raise ValueError('The graph_file option is mandatory')
		with open(self.graph_file, 'r') as f:
			self.graph = json.loads(json.load(f))

	def execute(self, quals, columns):
		district = None
		cause = None
		effect = None
		for qual in quals:
			#Might support lists later on
			if qual.field_name == 'district':
				if qual.operator == '=':
					district = qual.value
			if qual.field_name == 'cause':
				if qual.operator == '=':
					cause = qual.value
			if qual.field_name == 'effect':
				if qual.operator == '=':
					effect = qual.value
			''' Update when more columns are in place
			if qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				if qual.operator == '<':
					y_max = float(qual.value)
				if qual.operator == '==':
					y_min = float(qual.value)
					y_max = y_min
				if qual.operator == '<=':
					y_max = float(qual.value)
				if qual.operator == '>=':
					y_min = float(qual.value)
			'''
		

		log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)
		

		#initialize
		classes = ['IDP Drought', 'Precipitation', 'Food Prices', 'Livestock Prices', 'Water Prices', 'Fatalities']
		dict_list = self.graph
		result_dict = {}

		#TODO; Fix logic for cause and effect pair

		#iterate over districts
		if district is None:
			for d in dict_list:
				key = list(d.keys())[0]
				links_list = d[key]['graph']
				hasCause = {}
				hasEffect = {}
				#initialize values as empty lists
				for c in classes:
					hasCause[c] = []
					hasEffect[c] = []
				for i in range(len(links_list)):
					class_links = links_list[i]
					for j in range(len(class_links)):
						links = class_links[j]
						for link in links:
							#check if there is a lag
							if link == "-->":
								hasCause[classes[j]].append(classes[i])
								hasEffect[classes[i]].append(classes[j])
								break
				result_dict[key] = [hasCause, hasEffect]
		#find causality of specific district
		else:
			hasCause = {}
			hasEffect = {}
			#initialize values as empty lists
			for c in classes:
				hasCause[c] = []
				hasEffect[c] = []
			for d in dict_list:
				key = list(d.keys())[0]
				if key == district:
					links_list = d[key]['graph']
					for i in range(len(links_list)):
						class_links = links_list[i]
						for j in range(len(class_links)):
							links = class_links[j]
							for link in links:
								#check if there is a lag
								if link == "-->":
									hasCause[classes[j]].append(classes[i])
									hasEffect[classes[i]].append(classes[j])
									break
					break
			result_dict[district] = [hasCause, hasEffect]
		

		log_to_postgres('[SEMCUBE] Start yielding', logging.DEBUG)

		
		#formulate result line
		line = {}
		for res in result_dict:
			log_to_postgres('Result is: ' + str(result_dict[res]), logging.DEBUG)
			line['district'] = res
			#TODO; Support multiple causes and effects
			if cause is not None and effect is not None:
				#keep only related classes
				line['cause'] = cause
				line['effect'] = effect
				line['has_cause'] = {effect : result_dict[res][0][effect]}
				line['has_effect'] = {cause : result_dict[res][1][cause]}
			elif cause is not None:
				#keep only related classes
				cause_dict = result_dict[res][0]
				for r_class in list(cause_dict.keys()):
					if cause not in cause_dict[r_class]:
						del cause_dict[r_class]
				line['cause'] = cause
				line['effect'] = "*"
				line['has_cause'] = cause_dict
				line['has_effect'] = {cause : result_dict[res][1][cause]}
			elif effect is not None:
				#keep only related classes
				effect_dict = result_dict[res][1].copy()
				for r_class in list(effect_dict.keys()):
					if effect not in effect_dict[r_class]:
						del effect_dict[r_class]
				line['cause'] = "*"
				line['effect'] = effect
				line['has_cause'] = {effect : result_dict[res][0][effect]}
				line['has_effect'] = effect_dict
			else:
				line['cause'] = "*"
				line['effect'] = "*"
				line['has_cause'] = result_dict[res][0]
				line['has_effect'] = result_dict[res][1]
			log_to_postgres('Line is: ' + str(line), logging.DEBUG)
			yield line
			log_to_postgres('[SEMCUBE] Yielded line\n', logging.DEBUG)
		
		
