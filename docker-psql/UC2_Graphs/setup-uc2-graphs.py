import subprocess
from setuptools import setup, find_packages, Extension

setup(
  name='myfdw-uc2-graphs',
  version='0.0.1',
  author='Pakicious',
  license='Postgresql',
  packages=['myfdw-uc2-graphs'],
  install_requires=['xarray', 'zarr']
)
