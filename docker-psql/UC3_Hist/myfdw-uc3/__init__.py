import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres
import logging
import xarray as xr
from datetime import datetime, timedelta
import fsspec

#self.zarr_file -> uc3/uc3cube.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_dataset(self.zarr_file)
			
	def get_variable_name(self, columns):
		result = None
		for column_name in columns:
			log_to_postgres('[SEMCUBE] Column: ' + str(column_name), logging.DEBUG)
			if (column_name == "x"):
				continue;
			if (column_name == "y"):
				continue;
			if (column_name == "time"):
				continue;
			if (column_name == "evi"):
				result = "1 km 16 days EVI"
			elif (column_name == "ndvi"):
				result = "1 km 16 days NDVI"
			elif (column_name == "quality"):
				result = "1 km 16 days VI Quality"
			elif (column_name == "et_500m"):
				result = "ET_500m"
			elif (column_name == "et_qc_500m"):
				result = "ET_QC_500m"
			elif (column_name == "fparextra_qc"):
				result = "FparExtra_QC"
			elif (column_name == "fparlai_qc"):
				result = "FparLai_QC"
			elif (column_name == "fparstddev_500m"):
				result = "FparStdDev_500m"
			elif (column_name == "fpar_500m"):
				result = "Fpar_500m"
			elif (column_name == "le_500m"):
				result = "LE_500m"
			elif (column_name == "lst_day_1km"):
				result = "LST_Day_1km"
			elif (column_name == "lst_night_1km"):
				result = "LST_Night_1km"
			elif (column_name == "laistddev_500m"):
				result = "LaiStdDev_500m"
			elif (column_name == "lai_500m"):
				result = "Lai_500m"
			elif (column_name == "pet_500m"):
				result = "PET_500m"
			elif (column_name == "ple_500m"):
				result = "PLE_500m"
			elif (column_name == "qc_day"):
				result = "QC_Day"
			elif (column_name == "qc_night"):
				result = "QC_Night"
			elif (column_name == "aspect_max"):
				result = "aspect_max"
			elif (column_name == "aspect_mean"):
				result = "aspect_mean"
			elif (column_name == "aspect_min"):
				result = "aspect_min"
			elif (column_name == "aspect_std"):
				result = "aspect_std"
			elif (column_name == "burned_areas"):
				result = "burned_areas"
			elif (column_name == "clc_2006"):
				result = "clc_2006"
			elif (column_name == "clc_2012"):
				result = "clc_2012"
			elif (column_name == "clc_2018"):
				result = "clc_2018"
			elif (column_name == "dem_max"):
				result = "dem_max"
			elif (column_name == "dem_mean"):
				result = "dem_mean"
			elif (column_name == "dem_min"):
				result = "dem_min"
			elif (column_name == "dem_std"):
				result = "dem_std"
			elif (column_name == "era5_max_t2m"):
				result = "era5_max_t2m"
			elif (column_name == "era5_max_tp"):
				result = "era5_max_tp"
			elif (column_name == "era5_max_u10"):
				result = "era5_max_u10"
			elif (column_name == "era5_max_v10"):
				result = "era5_max_v10"
			elif (column_name == "era5_min_t2m"):
				result = "era5_min_t2m"
			elif (column_name == "era5_min_tp"):
				result = "era5_min_tp"
			elif (column_name == "era5_min_u10"):
				result = "era5_min_u10"
			elif (column_name == "era5_min_v10"):
				result = "era5_min_v10"
			elif (column_name == "fwi"):
				result = "fwi"
			elif (column_name == "ignition_points"):
				result = "ignition_points"
			elif (column_name == "number_of_fires"):
				result = "number_of_fires"
			elif (column_name == "population_density_2009"):
				result = "population_density_2009"
			elif (column_name == "population_density_2010"):
				result = "population_density_2010"
			elif (column_name == "population_density_2011"):
				result = "population_density_2011"
			elif (column_name == "population_density_2012"):
				result = "population_density_2012"
			elif (column_name == "population_density_2013"):
				result = "population_density_2013"
			elif (column_name == "population_density_2014"):
				result = "population_density_2014"
			elif (column_name == "population_density_2015"):
				result = "population_density_2015"
			elif (column_name == "population_density_2016"):
				result = "population_density_2016"
			elif (column_name == "population_density_2017"):
				result = "population_density_2017"
			elif (column_name == "population_density_2018"):
				result = "population_density_2018"
			elif (column_name == "population_density_2019"):
				result = "population_density_2019"
			elif (column_name == "population_density_2020"):
				result = "population_density_2020"
			elif (column_name == "roads_density_2020"):
				result = "roads_density_2020"
			elif (column_name == "slope_max"):
				result = "slope_max"
			elif (column_name == "slope_mean"):
				result = "slope_mean"
			elif (column_name == "slope_min"):
				result = "slope_min"
			elif (column_name == "slope_std"):
				result = "slope_std"
			else:
				raise ValueError('Unknown Column: ' + column_name)
		return result

	def execute(self, quals, columns):
		#ds = xr.open_dataset(self.zarr_file)

		#y and x values for Greece
		#y_min = 19.33
		y_min = 34.93
		y_max = 41.62
		x_min = 19.86
		x_max = 28.19
		variable_name = self.get_variable_name(columns)
		log_to_postgres('[SEMCUBE] Variable Name: ' + str(variable_name), logging.DEBUG)
		# 2009-03-06 ... 2020-12-26
		start_date = datetime.strptime("2009-03-06", "%Y-%m-%d")
		end_date = datetime.strptime("2020-12-26", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			#TODO; Handle <= and >= cases 
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				if qual.operator == '<':
					x_max = float(qual.value)
				if qual.operator == '==':
					x_min = float(qual.value)
					x_max = x_min
				if qual.operator == '<=':
					x_max = float(qual.value)
				if qual.operator == '>=':
					x_min = float(qual.value)
			if qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				if qual.operator == '<':
					y_max = float(qual.value)
				if qual.operator == '==':
					y_min = float(qual.value)
					y_max = y_min
				if qual.operator == '<=':
					y_max = float(qual.value)
				if qual.operator == '>=':
					y_min = float(qual.value)
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				if qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				if qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
				if qual.operator == '>=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				if qual.operator == '<=':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
		#check for static variables (ignore time dimension)
		time_static_vars = {'x', 'y', 'aspect_max', 'aspect_min', 'aspect_mean', 'aspect_std', 'clc_2006', 'clc_2006', 'clc_2012', 'clc_2018', 'dem_max','dem_min', 'dem_mean', 'dem_std', 'population_density_2009', 'population_density_2010', 'population_density_2011', 'population_density_2012', 'population_density_2013', 'population_density_2014', 'population_density_2015', 'population_density_2016', 'population_density_2017', 'population_density_2018', 'population_density_2019', 'population_density_2020', 'roads_density_2020', 'slope_max', 'slope_min', 'slope_mean', 'slope_std'}
		if columns.issubset(time_static_vars):
			end_date = start_date
		#check for static variables (ignore latitude, longitude dimensions)
		coords_static_vars = {'time', 'number_of_fires'}
		if columns.issubset(coords_static_vars):
			x_max = x_min
			y_max = y_min
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = self.ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
		
		variable_is_time_static = (variable_name in time_static_vars)
		variable_is_coords_static = (variable_name in coords_static_vars)
		
		x_size = sliced_ds.x.size
		y_size = sliced_ds.y.size

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
			i = 0 #time index is always 0
			time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
			#time = datetime.strptime(ds_subset.time.values[i], "%Y-%m-%dT%H:%M:%S")
			#return time in this form, otherwise postgres checks f an equality constraint on time is valid and does not return any results
			
			
			log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)
			
			value_array = None
			if variable_name is not None:
				value_array = ds_subset.get(variable_name)


			log_to_postgres('[SEMCUBE] Start yielding', logging.DEBUG)
			for j in range(y_size):
				yval = ds_subset.y.values[j]
				for k in range(x_size):
					log_to_postgres('[SEMCUBE] loop for i: ' + str(i) + ' k:' + str(k) + ' j:' + str(j), logging.DEBUG)
					line = {}
					value = None
					if value_array is not None:
						if variable_is_time_static:
							value = value_array.values[j][k]
						elif variable_is_coords_static:
							value = value_array.values[i]
						else:
							value = value_array.values[i][j][k]
						if numpy.isnan(value):
							continue
					#formulate result line
					for column_name in columns:
						if (column_name == "time"):
							line[column_name] = time
						elif (column_name == "x"):
							line[column_name] = ds_subset.x.values[k]
						elif (column_name == "y"):
							line[column_name] = yval
						else:
							line[column_name] = value
					yield line
					log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)
