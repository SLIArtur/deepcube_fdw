from gzip import READ
from sqlite3 import adapt
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres

import logging
import xarray as xr
from datetime import datetime, timedelta
from os.path import exists
from ftplib import FTP_TLS
from io import BytesIO
import h5netcdf

#self.fipool -> Fipool Directory

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.fipool_creds = fdw_options.get('fipool_creds', None)
		if self.fipool_creds is None:
			raise ValueError('The fipool_creds option is mandatory')

	def execute(self, quals, columns):

		with open(self.fipool_creds) as f:
			lines = f.readlines()
			lines = [line.rstrip() for line in lines]
			ip = lines[0].split(':')[1][1:]
			username = lines[1].split(":")[1][1:]
			password = lines[2].split(":")[1][1:]

		ftps = FTP_TLS(ip)
		#login after securing control channel
		ftps.login(username, password)
		#switch to secure data connection.
		#IMPORTANT! Otherwise, only the user and password is encrypted and not all the file data.
		ftps.prot_p()
		#ftps.retrlines('LIST')


		#y and x bounds (allegedly)
		y_min = 32.67304893926452
		y_max = 43.50010410387111
		x_min = 17.14768747179123
		x_max = 34.38954374661296

		#2022-07-01 ... 2022-10-31
		start_date = datetime.strptime("2022-07-08", "%Y-%m-%d")
		end_date = datetime.strptime("2022-10-31", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)

		#Variables for time calculations
		op_index = 0
		req_month = ""
		req_day = ""

		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				elif qual.operator == '<':
					x_max = float(qual.value)
			elif qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				elif qual.operator == '<':
					y_max = float(qual.value)
			elif qual.field_name == 'time':
				if qual.operator == '>':
					op_index = 1
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				elif qual.operator == ">=":
					op_index = 2
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '<':
					op_index = 3
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				elif qual.operator == "<=":
					op_index = 4
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '=':
					op_index = 5
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)


		#find netcdf files to retrieve requested vars
		if op_index <= 2:
			req_year = str(start_date.year)
			req_month = str(start_date.month).zfill(2)
			req_day = str(start_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			reqday_path = '/fipool' + '/' + req_year + '/' + req_month + '/' + reqfile_day
			reqnight_path = '/fipool' + '/' + req_year + '/' + req_month + '/' + reqfile_night
			months_list = ftps.nlst('/fipool' + '/' + req_year)
			#check if file path exists
			while ('/fipool' + '/' + req_year + '/' + req_month) in months_list:
				days_list = ftps.nlst('/fipool' + '/' + req_year + '/' + req_month)
				if (reqday_path in days_list) and (reqnight_path in days_list):
					#read files
					r_d = BytesIO()
					r_n = BytesIO()
					ftps.retrbinary('RETR ' + reqday_path, r_d.write)
					ftps.retrbinary('RETR ' + reqnight_path, r_n.write)
					#open, process and yield results
					yield from self.yield_lines(r_d, r_n, start_date, columns)
				else:
					log_to_postgres('[SEMCUBE] Requested day does not exist', logging.DEBUG)
				start_date = start_date + timedelta(days=1)
				req_month = str(start_date.month).zfill(2)
				req_day = str(start_date.day).zfill(2)
				reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
				reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
				reqday_path = '/fipool/2022/' + req_month + '/' + reqfile_day
				reqnight_path = '/fipool/2022/' + req_month + '/' + reqfile_night
			ftps.close()

		elif op_index == 3 or op_index == 4:
			req_year = str(end_date.year)
			req_month = str(end_date.month).zfill(2)
			req_day = str(end_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			reqday_path = '/fipool' + '/' + req_year + '/' + req_month + '/' + reqfile_day
			reqnight_path = '/fipool' + '/' + req_year + '/' + req_month + '/' + reqfile_night
			months_list = ftps.nlst('/fipool' + '/' + req_year)
			#check if file path exists
			while ('/fipool' + '/' + req_year + '/' + req_month) in months_list:
				days_list = ftps.nlst('/fipool' + '/' + req_year + '/' + req_month)
				if (reqday_path in days_list) and (reqnight_path in days_list):
					#read files
					r_d = BytesIO()
					r_n = BytesIO()
					ftps.retrbinary('RETR ' + reqday_path, r_d.write)
					ftps.retrbinary('RETR ' + reqnight_path, r_n.write)
					#open, process and yield results
					yield from self.yield_lines(r_d, r_n, end_date, columns)
				else:
					log_to_postgres('[SEMCUBE] Requested day does not exist', logging.DEBUG)
				end_date = end_date - timedelta(days=1)
				req_month = str(end_date.month).zfill(2)
				req_day = str(end_date.day).zfill(2)
				reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
				reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
				reqday_path = '/fipool/2022/' + req_month + '/' + reqfile_day
				reqnight_path = '/fipool/2022/' + req_month + '/' + reqfile_night
			ftps.close()

		elif op_index == 5:
			req_year = str(start_date.year)
			req_month = str(start_date.month).zfill(2)
			req_day = str(start_date.day).zfill(2)
			reqfile_day = 'fireindex_ds_2022' + req_month + req_day + '_1200.nc'
			reqfile_night = 'fireindex_postds_2022' + req_month + req_day + '_1200.nc'
			reqday_path = '/fipool/2022/' + req_month + '/' + reqfile_day
			reqnight_path = '/fipool/2022/' + req_month + '/' + reqfile_night
			months_list = ftps.nlst('/fipool' + '/' + req_year)
			#check if file path exists
			if ('/fipool' + '/' + req_year + '/' + req_month) in months_list:
				days_list = ftps.nlst('/fipool' + '/' + req_year + '/' + req_month)
				if (reqday_path in days_list) and (reqnight_path in days_list):
					#read files
					r_d = BytesIO()
					r_n = BytesIO()
					ftps.retrbinary('RETR ' + reqday_path, r_d.write)
					ftps.retrbinary('RETR ' + reqnight_path, r_n.write)
					yield from self.yield_lines(r_d, r_n, start_date, columns)
				else:
					log_to_postgres('[SEMCUBE] Requested day does not exist', logging.DEBUG)
			else:
				log_to_postgres('[SEMCUBE] Requested month does not exist', logging.DEBUG)
			ftps.close()


	def yield_lines(self, r_day, r_night, curr_date, columns):
			#open files
			day_ds = xr.open_dataset(BytesIO(r_day.getvalue()))
			night_ds = xr.open_dataset(BytesIO(r_night.getvalue()))

			log_to_postgres('[SEMCUBE] OPENED DATASETS', logging.DEBUG)

			'''
			log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)

			#TODO; Check SRID, slice after transforming to 4326
			sliced_day_ds = day_ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
			sliced_night_ds = night_ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min))
			log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)
			'''
			i = 0				#time index is always 0
			log_to_postgres('[SEMCUBE] Start yielding', logging.DEBUG)
			for j in range(day_ds.y.size):
				for k in range(day_ds.x.size):
					log_to_postgres('[SEMCUBE] loop for i: ' + str(i) + ' k:' + str(k) + ' j:' + str(j), logging.DEBUG)
					line = {}
					for column_name in columns:
						if (column_name == "time"):
							line[column_name] = curr_date
						elif (column_name == "y"):
							line[column_name] = day_ds.y.values[j]
						elif (column_name == "x"):
							line[column_name] = day_ds.x.values[k]
						elif (column_name == "lst_day"):
							line[column_name] = day_ds["lst_day"].values[i][j][k]
						elif (column_name == "ndvi"):
							line[column_name] = day_ds["ndvi"].values[i][j][k]
						elif (column_name == "evi"):
							line[column_name] = day_ds["evi"].values[i][j][k]
						elif (column_name == "fpar"):
							line[column_name] = day_ds["fpar"].values[i][j][k]
						elif (column_name == "lai"):
							line[column_name] = day_ds["lai"].values[i][j][k]
						elif (column_name == "sma"):
							line[column_name] = day_ds["sma"].values[i][j][k]
						elif (column_name == "smi"):
							line[column_name] = day_ds["smi"].values[i][j][k]
						elif (column_name == "t2m_24h"):
							line[column_name] = day_ds["t2m_24h"].values[j][k]
						elif (column_name == "ws10_24h"):
							line[column_name] = day_ds["ws10_24h"].values[i][j][k]
						elif (column_name == "rh2m_24h"):
							line[column_name] = day_ds["rh2m_24h"].values[i][j][k]
						elif (column_name == "tp_24h"):
							line[column_name] = day_ds["tp_24h"].values[i][j][k]
						elif (column_name == "d2m_24h"):
							line[column_name] = day_ds["d2m_24h"].values[i][j][k]
						elif (column_name == "sp_24h"):
							line[column_name] = day_ds["sp_24h"].values[i][j][k]
						elif (column_name == "lst_night"):
							line[column_name] = night_ds["lst_night"].values[i][j][k]
						elif (column_name == "dummy"):
							line[column_name] = night_ds["dummy"].values[i][j][k]
						else:
							raise ValueError('Unknown Column: ' + column_name)
					log_to_postgres('[SEMCUBE] Yielding line' + str(line), logging.DEBUG)
					yield line
					#log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)
