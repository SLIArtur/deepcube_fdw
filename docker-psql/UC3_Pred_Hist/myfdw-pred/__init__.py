import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres

import logging
import xarray as xr
from datetime import datetime, timedelta
import fsspec

#self.zarr_file -> predictions.nc

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_dataset(self.zarr_file)

	def get_variable_name(self, columns):
		result = None
		for column_name in columns:
			log_to_postgres('[SEMCUBE] Column: ' + str(column_name), logging.DEBUG)
			if (column_name == "x"):
				continue;
			elif (column_name == "y"):
				continue;
			elif (column_name == "time"):
				continue;
			else:
				result = column_name
		return result

	def execute(self, quals, columns):
		#y and x values for Greece
		y_min = 34.93
		y_max = 41.62
		x_min = 19.86
		x_max = 28.19
		# 2020-07-01 ... 2020-08-31
		variable_name = self.get_variable_name(columns)
		log_to_postgres('[SEMCUBE] Variable Name: ' + str(variable_name), logging.DEBUG)
		start_date = datetime.strptime("2020-07-01", "%Y-%m-%d")
		end_date = datetime.strptime("2020-08-31", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'x':
				if qual.operator == '>':
					x_min = float(qual.value)
				if qual.operator == '<':
					x_max = float(qual.value)
				if qual.operator == '==':
					x_min = float(qual.value)
					x_max = x_min
			if qual.field_name == 'y':
				if qual.operator == '>':
					y_min = float(qual.value)
				if qual.operator == '<':
					y_max = float(qual.value)
				if qual.operator == '==':
					y_min = float(qual.value)
					y_max = y_min
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				if qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				if qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
				if qual.operator == '>=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				if qual.operator == '<=':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing x_min: ' + str(x_min) + ' x_max:' + str(x_max) + ' y_min:' + str(y_min) + ' y_max:' + str(y_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = self.ds.sel(x=slice(x_min,x_max),y=slice(y_max,y_min),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)

		x_size = sliced_ds.x.size
		y_size = sliced_ds.y.size

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
			i = 0 #time index is always 0
			time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
			#time = datetime.strptime(ds_subset.time.values[i], "%Y-%m-%dT%H:%M:%S")
			#return time in this form, otherwise postgres checks f an equality constraint on time is valid and does not return any results


			log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)
			
			value_array = None
			if variable_name is not None:
				value_array = ds_subset.get(variable_name)

			log_to_postgres('[SEMCUBE] Start yielding', logging.DEBUG)
			for j in range(y_size):
				yval = ds_subset.y.values[j]
				for k in range(x_size):
					log_to_postgres('[SEMCUBE] loop for i: ' + str(timeindex) + ' k:' + str(k) + ' j:' + str(j), logging.DEBUG)
					line = {}
					value = None
					if value_array is not None:
						value = value_array.values[i][j][k]
						if numpy.isnan(value):
							continue
					#formulate result line
					for column_name in columns:
						if (column_name == "time"):
							line[column_name] = time
						elif (column_name == "x"):
							line[column_name] = ds_subset.x.values[k]
						elif (column_name == "y"):
							line[column_name] = yval
						else:
							line[column_name] = value
					yield line
					log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)
