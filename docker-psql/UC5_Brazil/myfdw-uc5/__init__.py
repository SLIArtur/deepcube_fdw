import numpy
from multicorn import ForeignDataWrapper
from multicorn.utils import log_to_postgres

import logging
import xarray as xr
from datetime import datetime, timedelta
import fsspec

#self.zarr_file -> uc5/datacube_uc5.zarr

class CubeForeignDataWrapper(ForeignDataWrapper):

	def __init__(self, fdw_options, fdw_columns):
		super(CubeForeignDataWrapper, self).__init__(fdw_options, fdw_columns)
		self.columns = fdw_columns
		self.zarr_file = fdw_options.get('zarr_file', None)
		if self.zarr_file is None:
			raise ValueError('The zarr_file option is mandatory')
		self.ds = xr.open_zarr(self.zarr_file)
	
	def get_variable_name(self, columns):
		result = None
		for column_name in columns:
			log_to_postgres('[SEMCUBE] Column: ' + str(column_name), logging.DEBUG)
			if (column_name == "lat"):
				continue
			if (column_name == "lon"):
				continue
			if (column_name == "time"):
				continue
			if (column_name == "aster_gdem_dem"):
				result = "ASTER_GDEM_DEM"
			elif (column_name == "cams_ch4"):
				result = "CAMS_ch4"
			elif (column_name == "cams_co"):
				result = "CAMS_co"
			elif (column_name == "cams_co3"):
				result = "CAMS_co3"
			elif (column_name == "cams_no"):
				result = "CAMS_no"
			elif (column_name == "cams_no2"):
				result = "CAMS_no2"
			elif (column_name == "cams_so2"):
				result = "CAMS_so2"
			elif (column_name == "lc100"):
				result = "LC100"
			elif (column_name == "ndvi"):
				result = "NDVI"
			elif (column_name == "ndvi_unc"):
				result = "NDVI_unc"
			elif (column_name == "swi_001"):
				result = "SWI_001"
			elif (column_name == "swi_005"):
				result = "SWI_005"
			elif (column_name == "swi_010"):
				result = "SWI_010"
			elif (column_name == "swi_015"):
				result = "SWI_015"
			elif (column_name == "swi_020"):
				result = "SWI_020"
			elif (column_name == "swi_040"):
				result = "SWI_040"
			elif (column_name == "swi_060"):
				result = "SWI_060"
			elif (column_name == "swi_100"):
				result = "SWI_100"
			elif (column_name == "thermal_mrt"):
				result = "THERMAL_MRT"
			elif (column_name == "thermal_utci"):
				result = "THERMAL_UTCI"
			return result

	def execute(self, quals, columns):
		#ds = xr.open_zarr(self.zarr_file)

		#lat and lon bounds
		lat_min = -6.01
		lat_max = -0.99
		lon_min = -47.01
		lon_max = -41.99
		variable_name = self.get_variable_name(columns)
		log_to_postgres('[SEMCUBE] Variable Name: ' + str(variable_name), logging.DEBUG)

		# 2019-01-01 ... 2019-03-31
		start_date = datetime.strptime("2019-01-01", "%Y-%m-%d")
		end_date = datetime.strptime("2019-04-01", "%Y-%m-%d")
		log_to_postgres('[SEMCUBE] Start looking for filters', logging.DEBUG)
		#check for filter condition here in order to avoid unnecessary access to zarr
		for qual in quals:
			if qual.field_name == 'lon':
				if qual.operator == '>':
					lon_min = float(qual.value)
				elif qual.operator == '<':
					lon_max = float(qual.value)
				elif qual.operator == '==':
					lon_min = float(qual.value)
					lon_max = lon_min
				elif qual.operator == '>=':
					lon_min = float(qual.value)
				elif qual.operator == '<=':
					lon_max = float(qual.value)
			if qual.field_name == 'lat':
				if qual.operator == '>':
					lat_min = float(qual.value)
				elif qual.operator == '<':
					lat_max = float(qual.value)
				elif qual.operator == '==':
					lat_min = float(qual.value)
					lat_max = lat_min
				elif qual.operator == '>=':
					lat_min = float(qual.value)
				elif qual.operator == '<=':
					lat_max = float(qual.value)
			if qual.field_name == 'time':
				if qual.operator == '>':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=1)
				elif qual.operator == '<':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S") + timedelta(days=-1)
				elif qual.operator == '=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
					end_date = start_date + timedelta(minutes=1439)
				elif qual.operator == '>=':
					start_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
				elif qual.operator == '<=':
					end_date = datetime.strptime(qual.value, "%Y-%m-%dT%H:%M:%S")
		#check for static variables (ignore time dimension)
		time_static_vars = {'lat', 'lon', 'ASTER_GDEM_DEM', 'LC100'}
		if columns.issubset(time_static_vars):
			end_date = start_date
		#check for static variables (ignore latitude, longitude dimensions)
		coords_static_vars = {'time'}
		if columns.issubset(coords_static_vars):
			lon_max = lon_min
			lat_max = lat_min
		log_to_postgres('[SEMCUBE] Finished looking for filters', logging.DEBUG)
		log_to_postgres('[SEMCUBE] Slicing lon_min: ' + str(lon_min) + ' lon_max:' + str(lon_max) + ' lat_min:' + str(lat_min) + ' lat_max:' + str(lat_max) + ' start_date:' + str(start_date) + ' end_date:' + str(end_date)  , logging.DEBUG)
		sliced_ds = self.ds.sel(lon=slice(lon_min,lon_max),lat=slice(lat_min,lat_max),time=slice(start_date, end_date))
		log_to_postgres('[SEMCUBE] Finished Slicing', logging.DEBUG)

		variable_is_time_static = (variable_name in time_static_vars)
		variable_is_coords_static = (variable_name in coords_static_vars)
		
		lat_size  = sliced_ds.lat.size
		lon_size = sliced_ds.lon.size

		for timeindex in range(sliced_ds.time.size):
			log_to_postgres('[SEMCUBE] Sel timeindex: ' + str(timeindex) + ' out of:' + str(sliced_ds.time.size), logging.DEBUG)
			ds_subset = sliced_ds.isel(time=[timeindex]) #chunking day by day
			log_to_postgres('[SEMCUBE] Finished sel', logging.DEBUG)
			i = 0 #time index is always 0
			time = numpy.datetime_as_string(ds_subset.time.values[i], unit='s')
			#time = datetime.strptime(ds_subset.time.values[i], "%Y-%m-%dT%H:%M:%S")
			#return time in this form, otherwise postgres checks if an equality constraint on time is valid and does not return any results

			log_to_postgres('[SEMCUBE] Finished column assignments', logging.DEBUG)

			value_array = None
			if variable_name is not None:
				value_array = ds_subset.get(variable_name)

			log_to_postgres('[SEMCUBE] Start yielding', logging.DEBUG)
			for j in range(lat_size):
				latval = ds_subset.lat.values[j]
				for k in range(lon_size):
					log_to_postgres('[SEMCUBE] loop for i: ' + str(i) + ' k:' + str(k) + ' j:' + str(j), logging.DEBUG)
					line = {}
					value = None
					if value_array is not None:
						if variable_is_time_static:
							value = value_array.values[j][k]
						elif variable_is_coords_static:
							value = value_array.values[i]
						else:
							value = value_array.values[i][j][k]
						if numpy.isnan(value):
							continue
					#formulate result line
					for column_name in columns:
						if (column_name == "time"):
							line[column_name] = time
						elif (column_name == "lat"):
							line[column_name] = latval
						elif (column_name == "lon"):
							line[column_name] = ds_subset.lon.values[k]
						else:
							line[column_name] = value
					yield line
					log_to_postgres('[SEMCUBE] Yielded line', logging.DEBUG)