CREATE SERVER multicorn_cube_mbr
    FOREIGN DATA WRAPPER multicorn
    OPTIONS (wrapper 'myfdw-mbr.CubeForeignDataWrapper');
	
	
CREATE FOREIGN TABLE IF NOT EXISTS public.cubetable_mbr(
    "time" character varying NULL COLLATE pg_catalog."default",
    variable_name character varying NULL COLLATE pg_catalog."default",
    aggregate_function character varying NULL COLLATE pg_catalog."default",
    spatial_relation character varying NULL COLLATE pg_catalog."default",
    geom character varying NULL COLLATE pg_catalog."default",
    result double precision NULL
) SERVER multicorn_cube_mbr
    OPTIONS (zarr_file '/docker-psql_test/cubes/dataset_greece.nc');