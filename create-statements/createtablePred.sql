CREATE SERVER multicorn_cube_pred FOREIGN DATA WRAPPER multicorn
options (
    wrapper 'myfdw-pred.CubeForeignDataWrapper'
);


CREATE FOREIGN TABLE IF NOT EXISTS public.cubetable_pred(
    "time" character varying NULL COLLATE pg_catalog."default",
    y double precision NULL,
    x double precision NULL,
    rf_predictions double precision NOT NULL,
    lstm_predictions double precision NOT NULL,
    conv_predictions double precision NOT NULL,
    convlstm_predictions double precision NOT NULL
)
    SERVER multicorn_cube_pred
    OPTIONS (zarr_file '/docker-psql_test/cubes/predictions_2020.nc');
