CREATE SERVER multicorn_cube_uc5 FOREIGN DATA WRAPPER multicorn
options (
    wrapper 'myfdw-uc5.CubeForeignDataWrapper'
);


CREATE FOREIGN TABLE IF NOT EXISTS public.cubetable_uc5(
    "time" character varying NULL COLLATE pg_catalog."default",
    lat double precision NULL,
    lon double precision NULL,
    aster_gdem_dem double precision NOT NULL,
    cams_ch4 double precision NOT NULL,
    cams_co double precision NOT NULL,
    cams_co3 double precision NOT NULL,
    cams_no double precision NOT NULL,
    cams_no2 double precision NOT NULL,
    cams_so2 double precision NOT NULL,
    lc100 double precision NOT NULL,
    ndvi double precision NOT NULL,
    ndvi_unc double precision NOT NULL,
    swi_001 double precision NOT NULL,
    swi_005 double precision NOT NULL,
    swi_010 double precision NOT NULL,
    swi_015 double precision NOT NULL,
    swi_020 double precision NOT NULL,
    swi_040 double precision NOT NULL,
    swi_060 double precision NOT NULL,
    swi_100 double precision NOT NULL,
    thermal_mrt double precision NOT NULL,
    thermal_utci double precision NOT NULL
)
    SERVER multicorn_cube_uc5
    OPTIONS (zarr_file '/docker-psql_test/cubes/datacube_linear_200.zarr');