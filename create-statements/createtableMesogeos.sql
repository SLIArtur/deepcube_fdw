CREATE SERVER multicorn_cube_uc3_mesogeos FOREIGN DATA WRAPPER multicorn
options (
    wrapper 'myfdw-uc3-mesogeos.CubeForeignDataWrapper'
);

CREATE FOREIGN TABLE public.cubetable_uc3_mesogeos (
    "time" character varying NOT NULL,
    y double precision NOT NULL,
    x double precision NOT NULL,
    aspect double precision,
    burned_areas double precision,
    curvature double precision,
    d2m double precision,
    dem double precision,
    ignition_points double precision,
    lai double precision,
    lc_agriculture double precision,
    lc_forest double precision,
    lc_grassland double precision,
    lc_settlement double precision,
    lc_shrubland double precision,
    lc_sparse_vegetation double precision,
    lc_water_bodies double precision,
    lc_wetland double precision,
    lst_day double precision,
    lst_night double precision,
    ndvi double precision,
    population double precision,
    rh double precision,
    roads_distance double precision,
    slope double precision,
    smi double precision,
    sp double precision,
    ssrd double precision,
    t2m double precision,
    tp double precision,
    wind_direction double precision,
    wind_speed double precision
)
SERVER multicorn_cube_uc3_mesogeos
OPTIONS (
    zarr_url '/docker-psql_test/cubes/mesogeos_greece.nc'
);
