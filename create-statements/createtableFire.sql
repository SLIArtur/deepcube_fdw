CREATE SERVER multicorn_cube_fire
    FOREIGN DATA WRAPPER multicorn
    OPTIONS (wrapper 'myfdw-fire.CubeForeignDataWrapper');


CREATE FOREIGN TABLE IF NOT EXISTS public.cubetable_fire(
    "time" character varying NOT NULL COLLATE pg_catalog."default",
    y double precision NOT NULL,
    x double precision NOT NULL,
    lst_day double precision NULL,
    ndvi double precision NULL,
    evi double precision NULL,
    fpar double precision NULL,
    lai double precision NULL,
    sma double precision NULL,
    smi double precision NULL,
    t2m_24h double precision NULL,
    ws10_24h double precision NULL,
    rh2m_24h double precision NULL,
    tp_24h double precision NULL,
    d2m_24h double precision NULL,
    sp_24h double precision NULL,
    lst_night double precision NULL,
    dummy double precision NULL
)
    SERVER multicorn_cube_fire
    OPTIONS (fipool '/docker-psql_test/fipool/');