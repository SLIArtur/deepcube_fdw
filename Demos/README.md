# UC3
Query results from the fist version of the semantic data cube system for UC3 can be found in [this Sextant Deployment](http://test.strabon.di.uoa.gr/SextantOL3/?mapid=mgko82hkb6qhct3a_) (please wait a few seconds for the KML layers to be loaded).

In this map you can see the results of two different queries, as two different layers. 

## Layer Natura-NDVI
This layer combines information about a specific class of non-EO features (vector geometries) with an EO observation variable from the data cube. It performs a geospatial join (geof:sfWithin) between the two datasets. 

-For each natura area (vector geometries) compute the average Normalized DIfference Vegetation Index (NDVI) for a specific day (2020-07-15).
```sparql
PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?label (AVG(?value) as ?avgNDVI) ?wktNat
WHERE {
?natura a uc3:NaturaArea ;
    rdfs:label ?label ;
    geo:asWKT ?wktNat .
?rastercell a uc3:ObservationRasterCell ;
    geo:asWKT ?wktObs .
?ndvi a uc3:NDVI ;
    uc3:refersToObservationRC ?rastercell ;
    uc3:hasNDVI ?value ;
    uc3:hasAquisitionDate "2020-07-15T00:00:00"^^xsd:string .
FILTER(geof:sfWithin(?wktObs,?wktNat)).
}
GROUP BY ?label ?wktNat
ORDER BY ?avgNDVI
```
## Natura-NDVI-CONV
This layer combines information about a specific class of non-EO features (vector geometries) with an EO observation variable from the data cube and also with the results of a fire risk prediction method, which is also available in a second data cube. It performs two geospatial joins (geof:sfWithin). Each join is performed between the first dataset and one of the other two.

-For each natura area (vector geometries) compute the average NDVI for a specific day (2020-07-15), but only return results for which the fire prediction using the CONV method gives a value larger than 0.1 for some point inside the natura area.
```sparql
PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?label (AVG(?value) as ?avgNDVI) ?wktNat
WHERE {
?natura a uc3:NaturaArea ;
    rdfs:label ?label ;
    geo:asWKT ?wktNat .
?rastercell a uc3:ObservationRasterCell ;
    geo:asWKT ?wktObs .
?ndvi a uc3:NDVI ;
    uc3:refersToObservationRC ?rastercell ;
    uc3:hasNDVI ?value ;
    uc3:hasAquisitionDate "2020-07-15T00:00:00"^^xsd:string .
?rastercell2 a uc3:PredictionRasterCell ;
    geo:asWKT ?wktPred .
?conv a uc3:CONV-Prediction ;
    uc3:refersToPredictionRC ?rastercell2 ;
    uc3:hasCONV-value ?value2 ;
    uc3:hasAquisitionDate "2020-07-15T00:00:00"^^xsd:string .
BIND("2020-07-15T00:00:00"^^xsd:string as ?date).
FILTER(?value2 > 0.1).
FILTER(geof:sfWithin(?wktPred, ?wktNat)).
FILTER(geof:sfWithin(?wktObs, ?wktNat)).
}
GROUP BY ?label ?date ?wktNat
ORDER BY ?avgNDVI
```
