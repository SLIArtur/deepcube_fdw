[PrefixDeclaration]
:		http://deepcube-h2020.eu/fire-risk/ontology
qb:		http://purl.org/linked-data/cube#
geo:		http://www.opengis.net/ont/geosparql#
owl:		http://www.w3.org/2002/07/owl#
rdf:		http://www.w3.org/1999/02/22-rdf-syntax-ns#
xml:		http://www.w3.org/XML/1998/namespace
xsd:		http://www.w3.org/2001/XMLSchema#
fire:		http://deepcube-h2020.eu/fire-risk/ontology
obda:		https://w3id.org/obda/vocabulary#
rdfs:		http://www.w3.org/2000/01/rdf-schema#

[MappingDeclaration] @collection [[
mappingId	MAPID-RASTERCELL
target		:ObservationRasterCell/{y}/{x}/{time} a :#ObservationRasterCell ; geo:asWKT {geometry}^^geo:wktLiteral . 
source		select x, y, time, ST_AsText(ST_SetSRID(ST_MakePoint(x,y),4326)) as geometry from cubetable_uc3_mesogeos

mappingId	MAPID-AU
target		:#AdministrativeUnit/{per} a :#AdministrativeUnit ; :#hasName {per} ; geo:asWKT {g}^^geo:wktLiteral . 
source		select per, ST_AsText(geom) as g from greece_regions

mappingId	MAPID-GAS_STATION
target		:FireProneStructure/{osm_id} a :#FireProneStructure ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_traffic where fclass = 'fuel'

mappingId	MAPID-ROAD
target		:Road/{osm_id} a :#Road ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_roads

mappingId	MAPID-RAILWAY
target		:Railway/{osm_id} a :#Road ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_railways

mappingId	MAPID-WATER
target		:Water/{osm_id} a :#Water ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_water

mappingId	MAPID-WATERWAYS
target		:Waterway/{osm_id} a :#Water ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_waterways

mappingId	MAPID-BUILDING
target		:Settlement/{osm_id} a :#Settlement ; geo:asWKT {g}^^geo:wktLiteral . 
source		select osm_id, ST_AsText(geom) as g from greece_buildings

mappingId	MAPID-NDVI
target		:NDVI/{y}/{x}/{time} a :#NDVI ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasNDVI {ndvi}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, ndvi from cubetable_uc3_mesogeos

mappingId	MAPID-BURNEDAREAS
target		:BurnedAreas/{y}/{x}/{time} a :#BurnedAreas ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasAreaSize {burned_areas}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, burned_areas from cubetable_uc3_mesogeos

mappingId	MAPID-T2M
target		:T2M/{y}/{x}/{time} a :#T2M ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasT2M {t2m}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, t2m from cubetable_uc3_mesogeos

mappingId	MAPID-TP
target		:TP/{y}/{x}/{time} a :#TP ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasTP {tp}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, tp from cubetable_uc3_mesogeos

mappingId	MAPID-ASPECT
target		:Aspect/{y}/{x}/{time} a :#Aspect ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasAspect {aspect}^^xsd:double . 
source		select x, y, time, aspect from cubetable_uc3_mesogeos

mappingId	MAPID-LSTDAY
target		:LST_Day{y}/{x}/{time} a :#LST_Day ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLSTDay {lst_day}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lst_day from cubetable_uc3_mesogeos

mappingId	MAPID-LSTNIGHT
target		:LST_Night/{y}/{x}/{time} a :#LST_Night ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLSTNight {lst_night}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lst_night from cubetable_uc3_mesogeos

mappingId	MAPID-LAI
target		:LAI/{y}/{x}/{time} a :#LAI ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLAI {lai}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lai from cubetable_uc3_mesogeos

mappingId	MAPID-SLOPE
target		:Slope/{y}/{x}/{time} a :#Slope ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasSlope {slope}^^xsd:double . 
source		select x, y, time, slope from cubetable_uc3_mesogeos

mappingId	MAPID-DEM
target		:DEM/{y}/{x}/{time} a :#DEM ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasDEM {dem}^^xsd:double . 
source		select x, y, time, dem from cubetable_uc3_mesogeos

mappingId	MAPID-IGNITIONPOINTS
target		:IgnitionPoints/{y}/{x}/{time} a :#IgnitionPoints ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasIgnitionPoints {ignition_points}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, ignition_points from cubetable_uc3_mesogeos

mappingId	MAPID-POPULATION
target		:AdministrativeUnit/{y}/{x}/{time} a :#AdministrativeUnit ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasPopulationDensity {population}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, population from cubetable_uc3_mesogeos

mappingId	MAPID-ROADSDISTANCE
target		:AdministrativeUnit/{y}/{x}/{time} a :#AdministrativeUnit ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasRoadsDistance {roads_distance}^^xsd:double . 
source		select x, y, time, roads_distance from cubetable_uc3_mesogeos

mappingId	MAPID-TWEET
target		:Tweet/{url} a :#Tweet ; :#hasID {id}^^xsd:string ; :#hasURL {url}^^xsd:string ; :#hasKeyword {keyword}^^xsd:string ; :#hasPlace {place}^^xsd:string ; :#hasTimestamp {timestamp}^^xsd:string . 
source		select id, url, timestamp, keyword, place from twitter_uc3

mappingId	MAPID-NATURA
target		:NaturaArea/{code} a :#NaturaArea ; geo:asWKT {g}^^geo:wktLiteral ; :#hasAreaCode {code}^^xsd:string ; rdfs:label {name_latin}^^xsd:string . 
source		select code, ST_AsText(geom) as g, name_latin from greece_natura

mappingId	MAPID-MBR
target		:MBR/{time}/{variable_name}/{aggregate_function}/{spatial_relation}/{geom}/{result} a :#MBR ; :#hasResult {result}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string ; :#hasVariable {variable_name}^^xsd:string ; :#hasAggrFunction {aggregate_function}^^xsd:string ; :#hasSpatialRel {spatial_relation}^^xsd:string ; :#hasGeom {geom}^^geo:wktLiteral . 
source		select time, variable_name, aggregate_function, spatial_relation, geom, result from cubetable_mbr_mesogeos

mappingId	MAPID-RAPTOR
target		:Raptor/{time}/{variable_name}/{aggregate_function}/{spatial_relation}/{geom}/{result} a :#Raptor ; :#hasResult {result}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string ; :#hasVariable {variable_name}^^xsd:string ; :#hasAggrFunction {aggregate_function}^^xsd:string ; :#hasSpatialRel {spatial_relation}^^xsd:string ; :#hasGeom {geom}^^geo:wktLiteral . 
source		select time, variable_name, aggregate_function, spatial_relation, geom, result from cubetable_raptor_mesogeos

mappingId	MAPID-CURVATURE
target		:Curvature/{y}/{x}/{time} a :#Curvature ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasCurvature {curvature}^^xsd:double . 
source		select x, y, time, curvature from cubetable_uc3_mesogeos

mappingId	MAPID-D2M
target		:D2M/{y}/{x}/{time} a :#D2M ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasD2M {d2m}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, d2m from cubetable_uc3_mesogeos

mappingId	MAPID-LC_AGRICULTURE
target		:LC_Agriculture/{y}/{x}/{time} a :#LC_Agriculture ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_agriculture}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_agriculture from cubetable_uc3_mesogeos

mappingId	MAPID-LC_FOREST
target		:LC_Forest/{y}/{x}/{time} a :#LC_Forest ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_forest}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_forest from cubetable_uc3_mesogeos

mappingId	MAPID-LC_GRASSLAND
target		:LC_Grassland/{y}/{x}/{time} a :#LC_Grassland ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_grassland}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_grassland from cubetable_uc3_mesogeos

mappingId	MAPID-LC_SETTLEMENT
target		:LC_Settlement/{y}/{x}/{time} a :#LC_Settlement ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_settlement}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_settlement from cubetable_uc3_mesogeos

mappingId	MAPID-LC_SHRUBLAND
target		:LC_Shrubland/{y}/{x}/{time} a :#LC_Shrubland ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_shrubland}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_shrubland from cubetable_uc3_mesogeos

mappingId	MAPID-LC_SPARSEVEGETATION
target		:LC_SparseVegetation/{y}/{x}/{time} a :#LC_SparseVegetation ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_sparse_vegetation}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_sparse_vegetation from cubetable_uc3_mesogeos

mappingId	MAPID-LC_WATERBODIES
target		:LC_WaterBodies/{y}/{x}/{time} a :#LC_WaterBodies ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_water_bodies}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_water_bodies from cubetable_uc3_mesogeos

mappingId	MAPID-LC_WETLAND
target		:LC_Wetland/{y}/{x}/{time} a :#LC_Wetland ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasLandCover {lc_wetland}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, lc_wetland from cubetable_uc3_mesogeos

mappingId	MAPID-RH
target		:RH/{y}/{x}/{time} a :#RH ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasRH {rh}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, rh from cubetable_uc3_mesogeos

mappingId	MAPID-SMI
target		:SMI/{y}/{x}/{time} a :#SMI ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasSMI {smi}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, smi from cubetable_uc3_mesogeos

mappingId	MAPID-SP
target		:SP/{y}/{x}/{time} a :#SP ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasSP {sp}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, sp from cubetable_uc3_mesogeos

mappingId	MAPID-SSRD
target		:SSRD/{y}/{x}/{time} a :#SSRD ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasSSRD {ssrd}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, ssrd from cubetable_uc3_mesogeos

mappingId	MAPID-WINDDIRECTION
target		:Wind/{y}/{x}/{time} a :#Wind ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasDirection {wind_direction}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, wind_direction from cubetable_uc3_mesogeos

mappingId	MAPID-WINDSPEED
target		:Wind/{y}/{x}/{time} a :#Wind ; :#refersToObservationRC :ObservationRasterCell/{y}/{x}/{time} ; :#hasSpeed {wind_speed}^^xsd:double ; :#hasAcquisitionDate {time}^^xsd:string . 
source		select x, y, time, wind_speed from cubetable_uc3_mesogeos
]]

