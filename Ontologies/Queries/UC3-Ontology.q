### UC3 ###

PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>

SELECT ?label ?wktPred ?time ?value
WHERE {
?natura a uc3:NaturaArea ;
    rdfs:label ?label ;
    geo:asWKT ?wktNatura .
?rastercell a uc3:PredictionRasterCell ;
    geo:asWKT ?wktPred .
?pred a uc3:FirePred ;
    uc3:refersToPredictionRC ?rastercell ;
    uc3:hasFirePred ?value ;
    uc3:hasAquisitionDate ?time .
FILTER(geof:distance(?wktPred, ?wktNatura, uom:metre) < 5000) .
FILTER(?value > 0.75)
}
ORDER BY DESC(?value)

PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>

SELECT ?label ?wktPred ?time ?value
WHERE {
?poi a uc3:POI ;
    uc3:hasType ?type ;
    uc3:hasName ?name ;
    geo:asWKT ?wktPOI .
?rastercell a uc3:PredictionRasterCell ;
    geo:asWKT ?wktPred .
?pred a uc3:FirePred ;
    uc3:refersToPredictionRC ?rastercell ;
    uc3:hasFirePred ?value ;
    uc3:hasAquisitionDate ?time .
FILTER(geof:distance(?wktPred, ?wktPOI, uom:metre) < 5000) .
FILTER(?type = "archaeological"^^xsd:string)
FILTER(?value > 0.25)
}

PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>

SELECT ?wktObs ?valueRH
WHERE {
?rastercellObs a uc3:ObservationRasterCell ;
    geo:asWKT ?wktObs .
?rh2m a uc3:RH2M_24H ;
    uc3:refersToObservationRC ?rastercellObs ;
    uc3:hasRH2M ?valueRH ;
    uc3:hasAquisitionDate ?timeObs .
FILTER(?timeObs = "2022-12-23T12:00:00"^^xsd:string) .
FILTER(?valueRH < 0.30)
}

PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>
PREFIX uom: <http://www.opengis.net/def/uom/OGC/1.0/>

SELECT ?wktObs ?valueRH ?valueFP
WHERE {
?rastercellObs a uc3:ObservationRasterCell ;
    geo:asWKT ?wktObs .
?rh2m a uc3:RH2M_24H ;
    uc3:refersToObservationRC ?rastercellObs ;
    uc3:hasRH2M ?valueRH ;
    uc3:hasAquisitionDate ?timeObs .
?rastercellPred a uc3:PredictionRasterCell ;
    geo:asWKT ?wktPred .
?pred a uc3:FirePred ;
    uc3:refersToPredictionRC ?rastercellPred ;
    uc3:hasFirePred ?valueFP ;
    uc3:hasAquisitionDate ?timePred .
FILTER(geof:distance(?wktObs, ?wktPred, uom:metre) < 1) .
FILTER(?timeObs > "2022-12-23T12:00:00"^^xsd:string && ?timePred > "2022-12-23T12:00:00"^^xsd:string) .
FILTER(?valueRH < 0.30 && ?valueFP > 0.75)
}

PREFIX uc3: <http://deepcube-h2020.eu/fire-risk/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?wktPred ?valueFP
WHERE {
?rastercellPred a uc3:PredictionRasterCell ;
    geo:asWKT ?wktPred .
?pred a uc3:FirePred ;
    uc3:refersToPredictionRC ?rastercellPred ;
    uc3:hasFirePred ?valueFP ;
    uc3:hasAquisitionDate ?timePred .
FILTER(?timePred > "2022-12-21T12:00:00"^^xsd:string) .
FILTER(?valueFP > 0.75)
}