### UC5 - Slovenia ###

PREFIX uc5: <http://deepcube-h2020.eu/tourism/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?value
WHERE {
?cams a uc5:CAMS ;
    uc5:refersToRC ?rastercell ;
    uc5:hasCarbonMonoxide ?value ;
    uc5:hasAquisitionDate "2021-01-01T00:00:00"^^xsd:dateTime .
}


### UC5 - France ###

PREFIX uc5: <http://deepcube-h2020.eu/tourism/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?value
WHERE {
?cams a uc5:CAMS ;
    uc5:refersToRC ?rastercell ;
    uc5:hasNitrogenDioxide ?value ;
    uc5:hasAquisitionDate "2022-01-01T00:00:00"^^xsd:dateTime .
}
