### UC2 ###

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?name (AVG(?value) as ?avgFP)
WHERE {
?food a geo:FoodPrice ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm .
?adm uc2:hasName ?name
}
GROUP BY ?name
ORDER BY ?avgFP

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?name (SUM(?value) as ?totalFatalities)
WHERE {
?event a geo:Fatalities ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm .
?adm uc2:hasName ?name
}
GROUP BY ?name
ORDER BY DESC(?totalFatalities)

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?name (AVG(?value) as ?avgFP)
WHERE {
?food a geo:FoodPrice ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm ;
    uc2:hasAquisitionDate ?date .
?adm uc2:hasName ?name .
FILTER(?date > "2020-01-01 00:00:00"^^xsd:string)
}
GROUP BY ?name
HAVING (?avgFP > 0.45)
ORDER BY ?avgFP

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?name (SUM(?value) as ?totalFatalities)
WHERE {
?event a geo:Fatalities ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm ;
    uc2:hasAquisitionDate ?date .
?adm uc2:hasName ?name .
FILTER(?date > "2020-01-01 00:00:00"^^xsd:string && ?date < "2021-01-01 00:00:00"^^xsd:string)
}
GROUP BY ?name
HAVING (?totalFatalities > 500)
ORDER BY ?totalFatalities

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT (AVG(?value) as ?avgFP) ?district ?cause ?has_cause
WHERE {
?parameter a geo:Parameter ;
    uc2:cause ?cause ;
    uc2:hasCause ?has_cause ;
    uc2:hasName ?district .
?food a geo:FoodPrice ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm .
?adm uc2:hasName ?district .
FILTER(?cause = "Food Prices"^^xsd:string)
}
GROUP BY ?district ?cause ?has_cause
ORDER BY ?district

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT (AVG(?value) as ?avgFP) ?district ?effect ?has_effect
WHERE {
?parameter a geo:Parameter ;
    uc2:cause ?effect ;
    uc2:hasEffect ?has_effect ;
    uc2:hasName ?district .
?water a geo:WaterPrice ;
    geo:affects ?displacement ;
    geo:hasValue ?value .
?displacement uc2:refersTo ?adm .
?adm uc2:hasName ?district .
FILTER(?effect = "Water Prices"^^xsd:string)
}
GROUP BY ?district ?effect ?has_effect
ORDER BY ?district

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?district ?causal_variable ?causal_link ?mean ?total_mean
WHERE {
?corr a geo:Correlation ;
    uc2:hasName ?district ;
    uc2:causalVar ?causal_variable ;
    uc2:causalLink ?causal_link ;
    uc2:hasMean ?mean ;
    uc2:hasTotalMean ?total_mean ;
    uc2:hasAquisitionDate ?date .
FILTER(?causal_variable = "IDP Drought"^^xsd:string)
FILTER(?date > "2010-01-01 00:00:00"^^xsd:string && ?date < "2013-01-01 00:00:00"^^xsd:string)
}

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?district ?result ?geom
WHERE {
?join a uc2:Join;
    uc2:hasDistrict ?district ;
    uc2:hasVariable "Water Prices"^^xsd:string ;
    uc2:hasAggregate "max" ^^xsd:string ;
    uc2:hasResult ?result ;
    uc2:hasGeom ?geom .
}

PREFIX uc2: <http://deepcube-h2020.eu/migration/ontology#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof: <http://www.opengis.net/def/function/geosparql/>

SELECT ?district_a ?district_b ?diff ?geom_a ?geom_b
WHERE {
?join a uc2:JoinSimilar;
    uc2:hasDistrictA ?district_a ;
    uc2:hasDistrictB ?district_b ;
    uc2:hasVariable "Water Prices"^^xsd:string ;
    uc2:hasAggregate "max" ^^xsd:string ;
    uc2:hasTolerance ?tolerance ;
    uc2:hasDiff ?diff ;
    uc2:hasGeomA ?geom_a ;
    uc2:hasGeomB ?geom_b .
    FILTER(?tolerance = 0.25)
}
